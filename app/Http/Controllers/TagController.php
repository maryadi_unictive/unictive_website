<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tag;
use JWTAuth;

class TagController extends Controller
{
    /**
     * Function to create a new tag
     * required fields:
     * title = string max 50
     * tag_type :
     *  1 = menu_tag
     *  2 = submenu_tag
     *  3 = detailmenu_tag
     */
    public function createNewTag(Request $request) {
        $validator = Validator::make($request->all(), [
            'title'    => 'required|string|max:50',
            'tag_type' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $tag = Tag::create([
                    'title' => $request->get('title'),
                    'tag_type' => $request->get('tag_type'),
                    'user_id' => $user->id,
                ]);
                if($tag) {
                    return response()->json([
                        'message' => 'Tag successfully created',
                        'status'  => 'OK'
                    ],200);
                } else {
                    return response()->json([
                        'message' => 'Failed to create tag',
                        'status'  => 'ERROR'
                    ],200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }


    /**
     * Function to create a new tag
     * required fields:
     * - id of the tag
     * - title = string max 50
     * - tag_type :
     *      1 = menu_tag
     *      2 = submenu_tag
     *      3 = detailmenu_tag
     */
    public function editTag(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'       => 'required',
            'title'    => 'required|string|max:50',
            'tag_type' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $tag = Tag::find($request->get('id'));
                $tag->title = $request->get('title');
                $tag->tag_type = $request->get('tag_type');
                $tag->user_id = $user->id;
                if($tag->save()) {
                    return response()->json([
                        'message' => 'Tag successfully updated',
                        'status'  => 'OK'
                    ],200);
                } else {
                    return response()->json([
                        'message' => 'Failed to update tag',
                        'status'  => 'ERROR'
                    ],200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
     * Function to delete tag based on id
     */
    public function deleteTag(Request $request) {
        $res_failed = [
            'message' => 'Failed to delete tag',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $tag = Tag::find($request->get('id'));
                $tag->user_id = $user->id;
                if($tag->save()) {
                    \Log::info("save okie");
                    if($tag->delete()) {
                        return response()->json([
                            'message' => 'Tag successfully deleted',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json($res_failed, 200);
                    }
                } else {
                    return response()->json($res_failed, 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }
}
