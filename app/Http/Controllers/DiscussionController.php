<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Discussion;
use JWTAuth;

class DiscussionController extends Controller
{
    /**
     * Create new discussion or contact_us. This function can be accessed without JWT token
     * because anyone can contact or create discussion with Unictive
     * Request parameter:
     *    'first_name'          => 'required|max:50',
     *    'last_name'           => 'nullable|max:50',
     *    'company_name'        => 'required|max:50',
     *    'email'               => 'required|max:50',
     *    'phone'               => 'required|max:15',
     *    'project_description' => 'required|max:1000'
     */
    public function createDiscussion(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name'          => 'required|max:50',
            'last_name'           => 'nullable|max:50',
            'company_name'        => 'required|max:50',
            'email'               => 'required|max:50',
            'phone'               => 'required|max:15',
            'project_description' => 'required|max:1000'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $disccussion = Discussion::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'company_name' => $request->get('company_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'project_description' => $request->get('project_description'),
        ]);

        if($disccussion) {
            return response()->json([
                'message' => 'Disccussion successfully created',
                'status'  => 'OK'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed to create disccussion',
                'status'  => 'ERROR'
            ], 200);
        }
    }

    /**
     * Function to edit existing discussion
     * This function requires JWT token to be accessed
     */
    public function editDiscussion(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'                  => 'required',
            'pic_id'              => 'nullable',
            'first_name'          => 'required|max:50',
            'last_name'           => 'nullable|max:50',
            'company_name'        => 'required|max:50',
            'email'               => 'required|max:50',
            'phone'               => 'required|max:15',
            'project_description' => 'required|max:1000'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $discussion = Discussion::find($request->get('id'));
                if(!empty($discussion)) {
                    $discussion->pic_id = $request->get('pic_id');
                    $discussion->first_name = $request->get('first_name');
                    $discussion->last_name = $request->get('last_name');
                    $discussion->company_name = $request->get('company_name');
                    $discussion->email = $request->get('email');
                    $discussion->phone = $request->get('phone');
                    $discussion->project_description = $request->get('project_description');
                    $discussion->updated_by_id = $user->id;
                    if($discussion->save()) {
                        return response()->json([
                            'message' => 'Discussion successfully updated',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json([
                            'message' => 'Failed to update discussion',
                            'status'  => 'ERROR'
                        ],200);
                    }
                } else {
                    return response()->json([
                        'message' => 'Disscussion id not found',
                        'status'  => 'ERROR'
                    ], 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }


    /**
     * Function to delete existing discussion
     * This function requires JWT token to be accessed
     */
    public function deleteDiscussion(Request $request) {
        $res_failed = [
            'message' => 'Failed to delete discussion',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $discussion = Discussion::find($request->get('id'));
                $discussion->updated_by_id = $user->id;
                if($discussion->save()) {
                    if($discussion->delete()) {
                        return response()->json([
                            'message' => 'Discussion successfully deleted',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json($res_failed, 200);
                    }
                } else {
                    return response()->json($res_failed, 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
     * Function to get all of discussion from db
     * This function requires JWT token to be accessed
     */
    public function getAllDiscussion(Request $request) {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $discussion = Discussion::all();
                return response()->json([
                    'data' => $discussion,
                    'status'  => 'OK'
                ],200);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
     * Function to get one discussion from db using it's id
     * This function requires JWT token to be accessed
     */
    public function getDiscussionById(Request $request) {
        $res_failed = [
            'message' => 'Discussion not found',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $discussion = Discussion::find($request->get('id'));
        if(!empty($discussion)) {
            return response()->json([
                'data'    => $discussion,
                'status'  => 'OK'
            ], 200);
        } else {
            return response()->json($res_failed, 200);
        }
    }
}
