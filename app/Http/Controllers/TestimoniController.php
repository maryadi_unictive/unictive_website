<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimoni;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class TestimoniController extends Controller
{
    /**
     * Get all testimonies
     */
    public function getAllTestimonies() {
        $data = Testimoni::get();
        return response()->json($data, 200);
    }

    /**
     * Get testimonie by it's id
     * input :
     *  id => required
     */
    public function getTestimoniById(Request $request) {
        $res_failed = [
            'message' => 'Testimoni not found',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $testimoni = Testimoni::find($request->get('id'));
        if(!empty($testimoni)) {
            return response()->json([
                'data'    => $testimoni,
                'status'  => 'OK'
            ], 200);
        } else {
            return response()->json($res_failed, 200);
        }
    }

    /**
     * Create new testimoni. This function can be accessed without JWT token
     * Request parameter:
     * 'first_name'   => 'required|max:50',
     * 'last_name'    => 'max:50',
     * 'company_name' => 'required|max:50',
     * 'company_logo' => 'max:2048|mimes:jpg,jpeg,png,svg,ico',
     * 'testimo_text' => 'required|max:500'
     * 'publish' => 'max:1'
     */
    public function createTestimoni(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name'   => 'required|max:50',
            'last_name'    => 'max:50',
            'publish'      => 'max:1',
            'company_name' => 'required|max:50',
            'company_logo' => 'nullable|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'testimoni_text' => 'required|max:500'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        /**
         * Process the company_logo
         * if it's EMPTY then just put empty url
         * if it's NOT EMPTY then put logo url
         */
        $moveRes = '';
        $icon = $request->file('company_logo');
        if (!empty($icon)) {
            $iconName = time().'_'.$icon->getClientOriginalName(); //.'.'.$icon->getClientOriginalExtension();
            $moveRes = $icon->move(public_path(parent::testimoni_icon_path), $iconName);
        }

        $testimonie = Testimoni::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'publish' => $request->get('publish'),
            'company_name' => $request->get('company_name'),
            'company_logo_url' => $moveRes,
            'testimoni_text' => $request->get('testimoni_text')
        ]);

        if($testimonie) {
            return response()->json([
                'message' => 'Testimoni successfully created',
                'status'  => 'OK'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed to create testimoni',
                'status'  => 'ERROR'
            ], 200);
        }
    }

    /**
     * Edit testimoni by it's id
     * 
     */
    public function editTestimoni(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'           => 'required',
            'first_name'   => 'required|max:50',
            'last_name'    => 'max:50',
            'publish'      => 'max:1',
            'company_name' => 'required|max:50',
            'company_logo' => 'nullable|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'testimoni_text' => 'required|max:500'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $testimoni = Testimoni::find($request->get('id'));
        if(empty($testimoni)) {
            return response()->json([
                'message' => 'Testimoni not found',
                'status'  => 'ERROR'
            ], 200);
        }

        /**
         * Process the company_logo
         * if it's EMPTY then no need to update logo
         * if it's NOT EMPTY then put logo url and delete previous logo file
         */
        $moveRes = '';
        $icon = $request->file('company_logo');
        if (!empty($icon)) {
            $iconName = time().'_'.$icon->getClientOriginalName(); //.'.'.$icon->getClientOriginalExtension();
            $moveRes = $icon->move(public_path(parent::testimoni_icon_path), $iconName);
        }
       
        if (file_exists($moveRes)) {
            if (file_exists($testimoni->company_logo_url)) {
                unlink($testimoni->company_logo_url);
            }
            $testimoni->company_logo_url = $moveRes;
        }

        // Assign new value from ionic froentend to testimoni
        $testimoni->first_name = $request->get('first_name');
        $testimoni->last_name = $request->get('last_name');
        $testimoni->publish = $request->get('publish');
        $testimoni->company_name = $request->get('company_name');
        $testimoni->testimoni_text = $request->get('testimoni_text');
        if($testimoni->save()) {
            return response()->json([
                'message' => 'Testimoni successfully updated',
                'status'  => 'OK'
            ],200);
        } else {
            return response()->json([
                'message' => 'Failed to update testimoni',
                'status'  => 'ERROR'
            ],200);
        }
    }

    /**
     * Delete testimoni using id
     * This function needs JWT token
     */
    public function deleteTestimoni(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $testimoni = Testimoni::find($request->get('id'));
                if (!empty($testimoni)) {
                    if ($testimoni->delete()) {
                        return response()->json([
                            'message' => 'Testimoni succesfully deleted',
                            'status'  => 'OK'
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'message' => 'Testimoni not found',
                        'status'  => 'ERROR'
                    ], 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }
}
