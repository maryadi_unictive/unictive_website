<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public const menu_icon_path = 'menu'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'icons';
    public const testimoni_icon_path = 'testimonie'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'icons';
    public const submenu_icon_path = 'submenu'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'icons';
    public const submenu_bg_image_path = 'submenu'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'bg_image';
    public const detailmenu_icon_path = 'detailmenu'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'icons';
    public const detailmenu_bg_image_path = 'detailmenu'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'bg_image';
}
