<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Menu;
use JWTAuth;

class MenuController extends Controller
{
    /**
     * Function to create new menu on left side of unictive website
     * This function requires JWT token to be accessed
     * 
     */
    public function createNewMenu(Request $request) {
        $validator = Validator::make($request->all(), [
            'title'    => 'required',
            'tag_id'   => 'required',
            'icon'     => 'required|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'description' => 'max:500'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $menu = 0;
                $icon = $request->file('icon');
                $iconName = time().'_'.$icon->getClientOriginalName(); //.'.'.$icon->getClientOriginalExtension();
                $moveRes = $icon->move(public_path(parent::menu_icon_path), $iconName);
                if (file_exists($moveRes)) {
                    $menu = Menu::create([
                        'title' => $request->get('title'),
                        'tag_id' => $request->get('tag_id'),
                        'user_id' => $user->id,
                        'icon_url' => $moveRes,
                        'description' => $request->get('description')
                    ]);
                    if($menu) {
                        return response()->json([
                            'message' => 'Menu successfully created',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json([
                            'message' => 'Failed to create menu',
                            'status'  => 'ERROR'
                        ],200);
                    }
                } else {
                    return response()->json([
                        'message' => 'Failed to process menu icon',
                        'status'  => 'ERROR'
                    ],200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
     * Function to edit existing menu
     * This function requires JWT token to be accessed
     */
    public function editMenu(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'       => 'required',
            'title'    => 'required',
            'tag_id'   => 'required',
            'icon'     => 'required|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'description' => 'max:500'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $menu = 0;
                $icon = $request->file('icon');
                $iconName = time().'_'.$icon->getClientOriginalName(); //.'.'.$icon->getClientOriginalExtension();
                $moveRes = $icon->move(public_path(parent::menu_icon_path), $iconName);
                if (file_exists($moveRes)) {
                    $menu = Menu::find($request->get('id'));
                    if (file_exists($menu->icon_url)) {
                        unlink($menu->icon_url);
                    }
                    $menu->title = $request->get('title');
                    $menu->tag_id = $request->get('tag_id');
                    $menu->user_id = $user->id;
                    $menu->icon_url = $moveRes;
                    $menu->description = $request->get('description');
                    if($menu->save()) {
                        return response()->json([
                            'message' => 'Menu successfully updated',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json([
                            'message' => 'Failed to update Menu',
                            'status'  => 'ERROR'
                        ],200);
                    }
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }


    /**
     * Function to delete existing menu
     * This function requires JWT token to be accessed
     */
    public function deleteMenu(Request $request) {
        $res_failed = [
            'message' => 'Failed to delete menu',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $menu = Menu::find($request->get('id'));
                $menu->user_id = $user->id;
                if($menu->save()) {
                    if($menu->delete()) {
                        return response()->json([
                            'message' => 'Menu successfully deleted',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json($res_failed, 200);
                    }
                } else {
                    return response()->json($res_failed, 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }


    /**
     * Function to delete existing menu
     * This function requires JWT token to be accessed
     */
    public function findMenuById(Request $request) {
        $res_failed = [
            'message' => 'Failed to find menu',
            'status'  => 'ERROR'
        ];

        $validator = Validator::make($request->all(), [
            'id'       => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $menu = Menu::find($request->get('id'));
                if(!empty($menu)) {
                    return response()->json([
                        'data'    => $menu,
                        'status'  => 'OK'
                    ],200);
                } else {
                    return response()->json($res_failed, 200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    /**
     * Get main menu that will be displayed on the left side of unictive website
     * We can use this function WITHOUT jtw token
     */

    public function getMainMenu(Request $request) {
        $menu = Menu::all();
        return response()->json($menu, 200);
    }
}
