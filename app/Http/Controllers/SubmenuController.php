<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Submenu;
use JWTAuth;

class SubmenuController extends Controller
{
    /**
     * Function to create new sub menu on left side of unictive website
     * This function requires JWT token to be accessed
     * 
     */
    public function createNewSubMenu(Request $request) {
        $validator = Validator::make($request->all(), [
            'menu_id'     => 'required',
            'title'       => 'required',
            'tag_id'      => 'required',
            'icon'        => 'nullable|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'bg_image'    => 'nullable|max:2048|mimes:jpg,jpeg,png,svg,ico',
            'description' => 'required|max:1000'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $iconUrl = '';
                $icon = $request->file('icon');
                if (!empty($icon)) {
                    $iconName = time().'_'.$icon->getClientOriginalName();
                    $moveIcon = $icon->move(public_path(parent::menu_icon_path), $iconName);
                    if (file_exists($moveIcon)) { 
                        $iconUrl = $moveIcon;
                    }
                }
                
                $bgImageUrl = '';
                $bgImage = $request->file('bg_image');
                if (!empty($bgImage)) {
                    $bgName = time().'_'.$bgImage->getClientOriginalName();
                    $moveBg = $bgImage->move(public_path(parent::menu_icon_path), $bgName);
                    if (file_exists($moveBg)) { 
                        $bgImageUrl = $moveBg;
                    }
                }
                
                if (file_exists($moveRes)) {
                    $menu = Menu::create([
                        'title' => $request->get('title'),
                        'tag_id' => $request->get('tag_id'),
                        'user_id' => $user->id,
                        'icon_url' => $moveRes,
                        'description' => $request->get('description')
                    ]);
                    if($menu) {
                        return response()->json([
                            'message' => 'Menu successfully created',
                            'status'  => 'OK'
                        ],200);
                    } else {
                        return response()->json([
                            'message' => 'Failed to create menu',
                            'status'  => 'ERROR'
                        ],200);
                    }
                } else {
                    return response()->json([
                        'message' => 'Failed to process menu icon',
                        'status'  => 'ERROR'
                    ],200);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }
}
