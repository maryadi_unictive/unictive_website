<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimoniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes(); 
            $table->string('first_name', 50);
            $table->string('last_name', 50)->nullable();
            $table->boolean('publish')->default(true);
            $table->string('company_name', 50);
            $table->string('company_logo_url')->nullable();
            $table->text('testimoni_text', 1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonies');
    }
}
