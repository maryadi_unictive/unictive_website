<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeveralBgImageToDetailbmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_menus', function (Blueprint $table) {
            $table->string('background_1_image_url')->nullable();
            $table->string('background_2_image_url')->nullable();
            $table->string('background_3_image_url')->nullable();
            $table->string('video_url')->nullable();
            $table->string('video_1_url')->nullable();
            $table->string('video_2_url')->nullable();
            $table->string('video_3_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detailmenus', function (Blueprint $table) {
            //
        });
    }
}
