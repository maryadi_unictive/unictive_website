<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');

Route::get('get_all_testimonies', 'TestimoniController@getAllTestimonies');
Route::post('get_testimoni_by_id', 'TestimoniController@getTestimoniById');
Route::post('create_testimoni', 'TestimoniController@createTestimoni');
Route::post('edit_testimoni', 'TestimoniController@editTestimoni');

Route::post('get_main_menu', 'MenuController@getMainMenu');

Route::post('create_discussion', 'DiscussionController@createDiscussion');
Route::post('edit_discussion', 'DiscussionController@editDiscussion');


Route::group(['middleware' => 'jwt.verify'], function (){
    Route::post('register', 'UserController@register');
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::post('logout', 'UserController@logout');

    Route::post('delete_testimoni', 'TestimoniController@deleteTestimoni');

    Route::post('create_tag', 'TagController@createNewTag');
    Route::post('edit_tag', 'TagController@editTag');
    Route::post('delete_tag', 'TagController@deleteTag');

    Route::post('create_menu', 'MenuController@createNewMenu');
    Route::post('edit_menu', 'MenuController@editMenu');
    Route::post('delete_menu', 'MenuController@deleteMenu');
    Route::post('get_menu_by_id', 'MenuController@findMenuById');
});
