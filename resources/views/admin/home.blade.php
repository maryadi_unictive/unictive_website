<?php
 use Helpers\ViewHelper;
	$bootstrap = asset('assets/css');
	if(env('APP_ENV') == 'local') {
		\Log::info('ENV LOCAL');
	} else {
		\Log::info('ENV PROD');
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home Unictive</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo ViewHelper::assetLocator('unictive_icon.png', 'unictive_images'); ?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

</head>
<body>	
	<div id="unauthorized" style="display:none;"> You are not authorized to view content of this page</div>
	<div id="authorized" style="display:none;"> 
				<!-- Bootstrap NavBar -->
		<nav class="navbar navbar-expand-md navbar-dark bg-primary">
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand" href="#">
				<img src="<?php echo ViewHelper::assetLocator('unictive_icon.png', 'unictive_images'); ?>" width="30" height="30" class="d-inline-block align-top" alt="U">
				<span class="menu-collapsed" id="userName">Admin</span>
			</a>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#top"><span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#top"></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="javascript:void(0)" id="logOut">Logout</a>
					</li>
					<!-- This menu is hidden in bigger devices with d-sm-none. 
				The sidebar isn't proper for smaller screens imo, so this dropdown menu can keep all the useful sidebar itens exclusively for smaller screens  -->
					<li class="nav-item dropdown d-sm-block d-md-none">
						<a class="nav-link dropdown-toggle" href="#" id="smallerscreenmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Menu </a>
						<div class="dropdown-menu" aria-labelledby="smallerscreenmenu">
							<a class="dropdown-item" href="#top">hjsahgjsa</a>
							<a class="dropdown-item" href="#top">Profile</a>
							<a class="dropdown-item" href="#top">Tasks</a>
							<a class="dropdown-item" href="#top">Etc ...</a>
						</div>
					</li><!-- Smaller devices menu END -->
				</ul>
			</div>
		</nav><!-- NavBar END -->
		<!-- Bootstrap row -->
		<div class="row" id="body-row">
			<!-- Sidebar -->
			<div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
				<!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
				<!-- Bootstrap List Group -->
				<ul class="list-group">
					<!-- Separator with title -->
					<li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
						<small>Dashboard</small>
					</li>
					<!-- /END Separator -->
					<!-- Menu with submenu -->
					<a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="bg-info list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fa fa-home fa-fw mr-3"></span>
							<span class="menu-collapsed">Main Menu</span>
							<span class="submenu-icon ml-auto"></span>
						</div>
					</a>
					<!-- Submenu content -->
					<div id='submenu1' class="collapse sidebar-submenu">
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Home</span>
						</a>
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Product</span>
						</a>
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Company</span>
						</a>
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">etc</span>
						</a>
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">This field depends on menu api</span>
						</a>
					</div>
					<a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="bg-info list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fa fa-user fa-fw mr-3"></span>
							<span class="menu-collapsed">Admin</span>
							<span class="submenu-icon ml-auto"></span>
						</div>
					</a>
					<!-- Submenu content -->
					<div id='submenu2' class="collapse sidebar-submenu">
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Settings</span>
						</a>
						<a href="javascript:void(0)" id="addNewUserMenu" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Add New User</span>
						</a>
						<a href="#" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Add New Client</span>
						</a>
						<a href="javascript:void(0)" id="addNewMenu" class="list-group-item list-group-item-action bg-light text-dark">
							<span class="menu-collapsed">Add New Menu</span>
						</a>
					</div>
					<a href="#" class="bg-info list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fa fa-tasks fa-fw mr-3"></span>
							<span class="menu-collapsed">Wording</span>
						</div>
					</a>
					<!-- Separator with title -->
					<li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
						<small>OPTIONS</small>
					</li>
					<!-- /END Separator -->
					<a href="#" class="bg-info list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fas fa-phone-square fa-fw mr-3"></span>
							<span class="menu-collapsed">Contact Us List</span>
						</div>
					</a>
					<a href="#" class="bg-info list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fas fa-bell fa-fw mr-3"></span>
							<span class="menu-collapsed">Notification <span class="badge badge-pill badge-primary ml-2">5</span></span>
						</div>
					</a>
					<!-- Separator without title -->
					<li class="list-group-item sidebar-separator menu-collapsed"></li>
					<!-- /END Separator -->
					<a href="#" class="bg-info list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span class="fa fa-question fa-fw mr-3"></span>
							<span class="menu-collapsed">Help</span>
						</div>
					</a>
					<a href="#top" data-toggle="sidebar-colapse" class="bg-info list-group-item list-group-item-action d-flex align-items-center">
						<div class="d-flex w-100 justify-content-start align-items-center">
							<span id="collapse-icon" class="fa fa-2x mr-3"></span>
							<span id="collapse-text" class="menu-collapsed"></span>
						</div>
					</a>
				</ul><!-- List Group END-->
			</div><!-- sidebar-container END -->

			<!-- MAIN -->
			<div id="mainField" class="col p-4">
				<h1 class="display-8">UNICTIVE ADMIN PANEL</h1>
				<div class="card">
					<h5 class="card-header font-weight-light">Title</h5>
					<div class="card-body">
						<ul>
							<li>Content 1</li>
							<li>Content 1</li>
							<li>Content 1</li>
						</ul>
					</div>
				</div>
			</div><!-- Main Col END -->

			<!-- ADD NEW USER -->
			<div id="addNewUserField" class="container m-4" style="width: 60%; display:none;">				
				<div class="row">
					<h3 class="display-8">Add New User</h3>
				</div>
					<div class="row">
						<div class="col-4 col-sm-2 col-form-label">
							<label for="inputName">Name</label>
						</div>
						<div class="col-8 col-sm-10">
							<input type="text" class="form-control form-control-sm"  id="inputName" placeholder="Name">
						</div>
					</div>
					<div class="row">
						<div class="col-4 col-sm-2 col-form-label">
							<label for="inputEmail">Email</label>
						</div>
						<div class="col-8 col-sm-10">
							<input type="text" class="form-control form-control-sm"  id="inputEmail" placeholder="Email">
						</div>
					</div>
					<div class="row">
						<label for="inputPassword" class="col-4 col-sm-2 col-form-label">Password</label>
						<div class="col-8 col-sm-10">
							<input type="password" class="form-control form-control-sm" id="inputPassword" placeholder="Password">
						</div>
					</div>
					<div class="row">
						<label for="inputConfirmPassword" class="col-4 col-sm-2 col-form-label">Confirm Password</label>
						<div class="col-8 col-sm-10">
							<input type="password" class="form-control form-control-sm" id="inputConfirmPassword" placeholder="Confirm Password">
						</div>
					</div>
					<div class="text-right">
						<button id="submitNewUser" class="btn btn-primary">Submit</button> 
					</div>
					<div id="userSuccess" class="alert alert-primary m-3" role="alert" style="display:none;">
						User has been succesfully added
					</div>
					<div id="userFailed" class="alert alert-danger m-3" role="alert" style="display:none;">
						Failed to add new user
					</div>
			</div>
			<!-- END OF ADD NEW USER -->

			<!-- ADD NEW MENU FIELD -->
			<div id="addNewMenuField" class="container m-4" style="width: 60%; display:none;">				
				<div class="row">
					<h3 class="display-8">Add New Menu</h3>
				</div>
					<div class="row">
						<div class="col-4 col-sm-2 col-form-label">
							<label for="inputTitle">Title</label>
						</div>
						<div class="col-8 col-sm-10">
							<input type="text" class="form-control form-control-sm"  id="inputTitle" placeholder="Name">
						</div>
					</div>
					<div class="row">
						<div class="col-4 col-sm-2 col-form-label">
							<label for="inputEmail">Email</label>
						</div>
						<div class="col-8 col-sm-10">
							<input type="text" class="form-control form-control-sm"  id="inputEmail" placeholder="Email">
						</div>
					</div>
					<div class="row">
						<label for="inputPassword" class="col-4 col-sm-2 col-form-label">Password</label>
						<div class="col-8 col-sm-10">
							<input type="password" class="form-control form-control-sm" id="inputPassword" placeholder="Password">
						</div>
					</div>
					<div class="row">
						<label for="inputConfirmPassword" class="col-4 col-sm-2 col-form-label">Confirm Password</label>
						<div class="col-8 col-sm-10">
							<input type="password" class="form-control form-control-sm" id="inputConfirmPassword" placeholder="Confirm Password">
						</div>
					</div>
					<div class="text-right">
						<button id="submitNewUser" class="btn btn-primary">Submit</button> 
					</div>
					<div id="userSuccess" class="alert alert-primary m-3" role="alert" style="display:none;">
						User has been succesfully added
					</div>
					<div id="userFailed" class="alert alert-danger m-3" role="alert" style="display:none;">
						Failed to add new user
					</div>
			</div>
			<!-- END OF ADD NEW MENU -->

		</div><!-- body-row END -->

	</div>
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('jquery-3.2.1.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('popper.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('bootstrap.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('select2.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('tilt.jquery.min.js', 'assets/js'); ?>"></script>
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('main.js', 'assets/js'); ?>"></script>
	<!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<!--===============================================================================================-->
	<script>
	$( document ).ready(function() {
		//localStorage.removeItem("key");
		localStorage.setItem("current_field", "#mainField");
		
		// Authenticating user
		getAuthenticatedUser();
		// Hide submenus
		$('#body-row .collapse').collapse('hide'); 

		// Collapse/Expand icon
		$('#collapse-icon').addClass('fa-angle-double-left'); 

		// Collapse click
		$('[data-toggle=sidebar-colapse]').click(function() {
			SidebarCollapse();
		});

		$('#addNewMenu').on("click", function(event) {
			// Get current active field and hide it;
			$(localStorage.getItem('current_field')).css('display', 'none');

			// Display current active field and set current_field var
			$('#addNewMenuField').css('display', 'block');
			localStorage.setItem('current_field', '#addNewMenuField');
		});

		$('#submitNewUser').on("click", function() {
			data = {
				name: $('#inputName').val(),
				email: $('#inputEmail').val(),
				password: $('#inputPassword').val(),
				password_confirmation: $('#inputConfirmPassword').val()
			};

			$.ajax({
			type: 'POST',
			url: 'api/register',
			data: data,
			headers: { 'Authorization': 'Bearer '+ localStorage.getItem('atjwt') }
			}).done(function (resp) {
				console.log(resp);
				if (resp.token !== '') {
					$('#userSuccess').fadeTo(2000, 1, function() {
						$('#userSuccess').fadeOut(1500);
					});
				} else {
					$('#userFailed').fadeTo(2000, 1, function() {
						$('#userFailed').fadeOut(1500);
					});
				}
			}).fail(function (err) {
				$('#userFailed').html(err.responseJSON);
				$('#userFailed').fadeTo(2500, 1, function() {
						$('#userFailed').fadeOut(1500);
				});
			});
			
		});

		$('#logOut').on("click", function(event) {
			$.ajax({
			type: 'POST',
			url: 'api/logout',
			headers: { 'Authorization': 'Bearer '+ localStorage.getItem('atjwt') }
			}).done(function (resp) {
				if (resp.status == 'OK') {
					window.location.href = "{!! url('admin') !!}";
				}
			});
		});

		$('#addNewUserMenu').on("click", function(event) {
			// Get current active field and hide it;
			 $(localStorage.getItem('current_field')).css('display', 'none');

			// Display current active field and set current_field var
			$('#addNewUserField').css('display', 'block');
			localStorage.setItem('current_field', '#addNewUserField');
		});
	});

	function getAuthenticatedUser() {
		$.ajax({
			type: 'GET',
			url: 'api/user',
			headers: { 'Authorization': 'Bearer '+ localStorage.getItem('atjwt') }
		}).done(function (resp) {
			$('#unauthorized').css('display', 'none');
			$('#authorized').css('display', 'block');

			if (undefined !== resp.status) {
				$('#unauthorized').css('display', 'block');
				$('#authorized').css('display', 'none');
				$('#unauthorized').html("<b>"+ resp.status + "</b>"+"<br> You will be redirected to Login page in 3 seconds");
				setTimeout(function(){ 
					window.location.href = "{!! url('admin') !!}";
				}, 3000);
				
			}			
			$('#userName').html("Welcome <b>"+resp.user.name+"</b>");
		}).fail(function (err) {
			$('#unauthorized').css('display', 'block');
			$('#authorized').css('display', 'none');
			window.location.href = "{!! url('admin') !!}";
		});
	}

	function SidebarCollapse () {
		$('.menu-collapsed').toggleClass('d-none');
		$('.sidebar-submenu').toggleClass('d-none');
		$('.submenu-icon').toggleClass('d-none');
		$('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
		
		// Treating d-flex/d-none on separators with title
		var SeparatorTitle = $('.sidebar-separator-title');
		if ( SeparatorTitle.hasClass('d-flex') ) {
			SeparatorTitle.removeClass('d-flex');
		} else {
			SeparatorTitle.addClass('d-flex');
		}
		
		// Collapse/Expand icon
		$('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
	}



	</script>
</body>
	
</html>