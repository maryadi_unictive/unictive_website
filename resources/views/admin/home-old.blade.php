<?php
 use Helpers\ViewHelper;
	$bootstrap = asset('assets/css');
	if(env('APP_ENV') == 'local') {
		\Log::info('ENV LOCAL');
	} else {
		\Log::info('ENV PROD');
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home Unictive</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo ViewHelper::assetLocator('unictive_icon.png', 'unictive_images'); ?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('bootstrap.min.css', 'assets/css/bootstrap'); ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('font-awesome.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('animate.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('hamburgers.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('select2.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('main_home.css', 'assets/css'); ?>">

</head>
<body>	
	<div id="unauthorized" style="display:none;"> You are not authorized to view content of this page</div>
	<div id="authorized" style="display:none;"> 
		<div class="wrapper">

			<!-- Sidebar -->
			<nav id="sidebar">
				<div class="sidebar-header">
					<h3>Unictive Admin Panel</h3>
				</div>

				<ul class="list-unstyled components">
					<p>Dummy Heading</p>
					<li class="active">
						<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
						<ul class="collapse list-unstyled" id="homeSubmenu">
							<li>
								<a href="#">Home 1</a>
							</li>
							<li>
								<a href="#">Home 2</a>
							</li>
							<li>
								<a href="#">Home 3</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">About</a>
					</li>
					<li>
						<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
						<ul class="collapse list-unstyled" id="pageSubmenu">
							<li>
								<a href="#">Page 1</a>
							</li>
							<li>
								<a href="#">Page 2</a>
							</li>
							<li>
								<a href="#">Page 3</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Portfolio</a>
					</li>
					<li>
						<a href="#">Contact</a>
					</li>
				</ul>
			</nav>

			<!-- Page Content -->
			<div id="content">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<div class="container-fluid">

						<button type="button" id="sidebarCollapse" class="btn btn-info">
							<i class="fas fa-align-left"></i>
							<span>Toggle Sidebar</span>
						</button>

					</div>
				</nav>
			</div>

		</div> 
	</div>

	
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('jquery-3.2.1.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('popper.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('bootstrap.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('select2.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('tilt.jquery.min.js', 'assets/js'); ?>"></script>
<!--===============================================================================================-->
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('main.js', 'assets/js'); ?>"></script>
	<script>
	$( document ).ready(function() {
		//localStorage.removeItem("key");
		getAuthenticatedUser();
	});

	</script>
</body>
</html>