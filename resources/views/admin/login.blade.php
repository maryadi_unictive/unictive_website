<?php
 use Helpers\ViewHelper;
	$bootstrap = asset('assets/css');
	if(env('APP_ENV') == 'local') {
		\Log::info('ENV LOCAL');
	} else {
		\Log::info('ENV PROD');
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Unictive</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo ViewHelper::assetLocator('unictive_icon.png', 'unictive_images'); ?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('font-awesome.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('animate.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('hamburgers.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('select2.min.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('util.css', 'assets/css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::assetLocator('main.css', 'assets/css'); ?>">

</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo ViewHelper::assetLocator('final-unictive-logo.png', 'unictive_images'); ?>" alt="IMG">
				</div>

				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Admin Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id='login_btn'>
							Login
						</button>
					</div>
					<div class="text-center p-t-136">
						<a class="txt2" href="#"> Copyright © PT Uniktif Media Indonesia 2020</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('jquery-3.2.1.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('popper.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('bootstrap.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('select2.min.js', 'assets/js'); ?>"></script>
	<script src="<?php echo ViewHelper::assetLocator('tilt.jquery.min.js', 'assets/js'); ?>"></script>
<!--===============================================================================================-->
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->	
	<script src="<?php echo ViewHelper::assetLocator('main.js', 'assets/js'); ?>"></script>
	<script>
	$( document ).ready(function() {
		$('#login_btn').on('click', function(event) {
			event.preventDefault();
			data = {
				email: $('input[name="email"]').val(),
				password: $('input[name="pass"]').val()
			};
			$.ajax({
				type:'POST',
				url:'/api/login',
				data: data
			}).done(function (response) {
				//console.log(response.token);
				localStorage.setItem("atjwt", response.token);
				window.location.href = "{!! url('home') !!}";
				// $.ajax({
				// 	type: 'GET',
				// 	url: 'home',
				// 	headers: { 'Autorization': 'Bearer '+response.token }
				// }).done(function (resp) {
				// 	console.log('after get');
				// 	window.location.href = "{!! url('home') !!}";
				// });
			}).fail(function (error) {
				// @todo show error pop here
				console.log(error)
			});
		});
	});
	</script>
</body>
</html>