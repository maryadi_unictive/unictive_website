<!DOCTYPE html>
<html lang="en">
<head>
	<title>Admin</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/" >
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ url('').'/public/unictive_images/unictive_icon.png' }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/bootstrap/bootstrap.min.css' }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/font-awesome.min.css' }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/animate.css' }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/hamburgers.min.css' }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/select2.min.css' }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/util.css' }}">
	<link rel="stylesheet" type="text/css" href="{{ url('').'/public/assets/css/main.css' }}">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{ url('').'/public/unictive_images/final-unictive-logo.png' }}" alt="IMG">
				</div>

				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Admin Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{ url('').'/public/assets/js/jquery-3.2.1.min.js' }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('').'/public/assets/js/popper.js' }}"></script>
	<script src="{{ url('').'/public/assets/js/bootstrap.min.js' }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('').'/public/assets/js/select2.min.js' }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('').'/public/assets/js/tilt.jquery.min.js' }}"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{ url('').'/public/assets/js/main.js' }}"></script>

</body>
</html>