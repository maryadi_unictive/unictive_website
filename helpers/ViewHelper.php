<?php

namespace Helpers;

class ViewHelper {
    public static function shout(string $string)
    {
        \Log::info('we are in shout view helper');
        return strtoupper($string);
    }

    public static function assetLocator($assetName, $assetPath) {
        $retVal = '';
        if (env('APP_ENV') === 'local') {
            $retVal = asset($assetPath.'/'.$assetName);
        } else if (env('APP_ENV') === 'production') {
            $retVal = url('').'/public/'.$assetPath.'/'.$assetName;
        }
        return $retVal;
        //return asset($assetPath.'/'.$assetName);
    }
}